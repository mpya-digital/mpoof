const multipleEntry = require("react-app-rewire-multiple-entry")([
  {
    entry: "src/extension/linkedin-recruiter.ts",
  },
]);

module.exports = {
  webpack: function (config, env) {
    multipleEntry.addMultiEntry(config);

    config.output = {
      ...config.output,
      filename: (pathData) => {
        const fileName = pathData.chunk.name.split(".")[0];
        return `static/js/${fileName}.js`;
      },
    };

    return config;
  },
};
