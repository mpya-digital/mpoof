# MPOOF! :bomb:

A Chrome plugin that removes all profiles that have already been seen in LinkedIn Recruiter.

## Setup

1. Clone the project with `git clone`.
2. Run `npm install` to install all dependencies.
3. Run `npm start` to run the app in development mode at [http://localhost:3000](http://localhost:3000).

### Test it as an extension

4. Build the project with `npm run build`.
5. In Chrome, enter [chrome://extensions](chrome://extensions).
6. Select **Developer mode**
7. Click **Load Package** and select the build folder in the project.
8. You can now see an icon for the extension next to your other extensions.

## Test

Run all unit tests with `npm test`.
