// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import "@testing-library/jest-dom";

Object.assign(global, require("jest-chrome"));

const localStorageMock = () => {
  let store: Record<string, string> = {};
  return {
    getItem: (key: string) => store[key],
    setItem: (key: string, value: string) => {
      store[key] = value;
    },
    clear: () => {
      store = {};
    },
  };
};

Object.defineProperty(global, "localStorage", { value: localStorageMock() });

(chrome as any).action = {
  setIcon: jest.fn(),
};
