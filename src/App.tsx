import { useEffect, useState } from "react";
import styles from "./App.module.scss";
import Header from "./components/header/Header";
import Main from "./components/main/Main";

export default function App() {
  const [visible, setVisbile] = useState(false);

  useEffect(() => {
    setVisbile(true);
  }, []);

  return (
    <div className={`${styles.container} ${visible ? styles.visible : ""}`}>
      <Header />
      <Main />
    </div>
  );
}
