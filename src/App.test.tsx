import { render } from "@testing-library/react";
import App from "./App";

describe("App", () => {
  it("should render component", () => {
    const app = render(<App />);
    expect(app.asFragment()).toMatchSnapshot();
  });

  it("should set opacity to 1 after 1s", async () => {
    const app = render(<App />);

    await new Promise((r) => setTimeout(r, 1500));

    expect(app.asFragment()).toMatchSnapshot();
  });
});
