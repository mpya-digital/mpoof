import {
  fireEvent,
  render,
  RenderResult,
  screen,
} from "@testing-library/react";
import { act } from "react-dom/test-utils";
import Main from "./Main";

jest.mock("../../extension/chrome", () => ({
  ...jest.requireActual("../../extension/chrome"),
  getTabUrl: Promise.resolve("https://www.linkedin.com"),
}));

describe("Main", () => {
  let main: RenderResult | undefined;

  it("should render button for showing/hiding candidates if url is linkedin.com", async () => {
    await act(async () => {
      main = render(<Main />);
    });

    expect(main?.asFragment()).toMatchSnapshot();
  });

  it("should render secondary button it button is clicked", async () => {
    await act(async () => {
      main = render(<Main />);
    });

    const button = screen.getByText(/Make the contacted ones poof!/i);
    fireEvent.click(button);

    await act(async () => {
      await new Promise((r) => setTimeout(r, 500));
    });

    expect(main?.asFragment()).toMatchSnapshot();
  });
});
