import { useEffect, useLayoutEffect, useRef, useState } from "react";
import {
  getTabUrl,
  updateExtension,
  updateTabUrl,
} from "../../extension/chrome";
import {
  extensionActiveInTest,
  EXTENSION_PERMITTED_URL,
} from "../../extension/constants";

import useLocalStorage, { Candidates } from "../../hooks/use-localstorage";
import Button from "../button/Button";
import Text from "../text/Text";
import styles from "./Main.module.scss";

export default function Main() {
  const [extensionActive, setExtensionActive] = useState(extensionActiveInTest);

  const [candidatesHidden, setCandidatesHidden] =
    useLocalStorage("candidatesHidden");

  const changeCandidatesHidden = (type: Candidates) => {
    setCandidatesHidden((state) => ({ ...state, [type]: !state[type] }));
  };

  const firstUpdate = useRef(true);

  useLayoutEffect(() => {
    if (!firstUpdate.current) {
      updateExtension(candidatesHidden);
    } else {
      firstUpdate.current = false;
    }
  }, [candidatesHidden]);

  useEffect(() => {
    (async () => {
      const tabUrl = await getTabUrl;
      if (tabUrl?.includes(EXTENSION_PERMITTED_URL)) {
        setExtensionActive(true);
      }
    })();
  }, []);

  return (
    <main className={styles.container}>
      {extensionActive ? (
        <>
          <Button
            onClick={() => {
              changeCandidatesHidden("seen");
            }}
            pressed={candidatesHidden.seen}
          >
            {!candidatesHidden.seen
              ? "Make the seen ones poof!"
              : "Take the seen ones back!"}
          </Button>

          <Button
            onClick={() => {
              changeCandidatesHidden("contacted");
            }}
            pressed={candidatesHidden.contacted}
          >
            {!candidatesHidden.contacted
              ? "Make the contacted ones poof!"
              : "Take the contacted ones back!"}
          </Button>
        </>
      ) : (
        <Text>
          This extension only works on{" "}
          <a href="#" onClick={updateTabUrl}>
            LinkedIn
          </a>
          .
        </Text>
      )}
    </main>
  );
}
