import { render, RenderResult } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import Button from "./Button";

describe("Button", () => {
  it("should render a button", () => {
    let button: RenderResult | undefined;

    act(() => {
      button = render(<Button onClick={jest.fn} pressed={true}></Button>);
    });

    expect(button?.asFragment()).toMatchSnapshot();
  });
});
