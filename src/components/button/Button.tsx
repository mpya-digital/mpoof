import { HTMLProps, PropsWithChildren, useEffect, useState } from "react";
import styles from "./Button.module.scss";

interface Props extends PropsWithChildren, HTMLProps<HTMLButtonElement> {
  pressed: boolean;
}

export default function Button({ children, pressed, onClick }: Props) {
  const [textState, setTextState] = useState({
    opacity: 0,
    text: children,
  });

  useEffect(() => {
    setTextState((state) => ({ ...state, opacity: 0 }));
    setTimeout(() => {
      setTextState({ text: children, opacity: 1 });
    }, 250);
  }, [children]);

  return (
    <button
      className={`${styles.button} ${pressed ? styles.pressed : ""}`}
      onClick={onClick}
    >
      <span
        className={`${styles.text} ${textState.opacity ? styles.visible : ""}`}
      >
        {textState.text}
      </span>
    </button>
  );
}
