import { PropsWithChildren } from "react";
import styles from "./Text.module.scss";

export default function Text({ children }: PropsWithChildren) {
  return <p className={styles.text}>{children}</p>;
}
