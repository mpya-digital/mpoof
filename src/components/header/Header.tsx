import styles from "./Header.module.scss";

export default function Header() {
  return (
    <header className={styles.container}>
      <h1 className={styles.title}>MPOOF</h1>
    </header>
  );
}
