import { renderHook } from "@testing-library/react";
import useLocalStorage from "./use-localstorage";

describe("useLocalStorage", () => {
  it("should set a localstorage item with default value", () => {
    localStorage.setItem(
      "candidatesHidden",
      JSON.stringify({ contacted: true, seen: false })
    );
    const { result } = renderHook(() => useLocalStorage("candidatesHidden"));

    const value = result.current[0];
    expect(value).toEqual({ contacted: true, seen: false });
  });
});
