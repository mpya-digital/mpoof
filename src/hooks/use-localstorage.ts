import { Dispatch, SetStateAction, useEffect, useState } from "react";

export type Candidates = "contacted" | "seen";

export type StorageName = "candidatesHidden";
export type StorageValue = Record<Candidates, boolean>;

const getInitalValue = (storageName: StorageName) =>
  localStorage.getItem(storageName);

export default function useLocalStorage(
  storageName: StorageName
): [StorageValue, Dispatch<SetStateAction<StorageValue>>] {
  const [storageValue, setStorageValue] = useState<StorageValue>(() => {
    const initalValue = getInitalValue(storageName);
    return initalValue ? JSON.parse(initalValue) : {};
  });

  useEffect(() => {
    localStorage.setItem(storageName, JSON.stringify(storageValue));
  }, [storageName, storageValue]);

  return [storageValue, setStorageValue];
}
