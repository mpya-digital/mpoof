export const extensionActiveInTest = process.env.NODE_ENV === "development";

export const EXTENSION_PERMITTED_URL = "https://www.linkedin.com";

export const CANDIDATES_LIST_CLASSES = ".ember-view.profile-list";
export const CANDIDATE_CONTACTED_CLASS =
  ".base-decoration__trigger--conversations";
export const CANDIDATE_SEEN_CLASS = ".base-decoration__trigger--views";

export const transitionDurationSec = 0.5;
