import { StorageValue } from "../hooks/use-localstorage";
import { EXTENSION_PERMITTED_URL } from "./constants";

export function updateExtension(candidatesHidden: StorageValue) {
  if (chrome.tabs) {
    chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
      const tabId = tabs[0].id;
      if (tabId) {
        chrome.tabs.sendMessage(tabId, candidatesHidden);
      }
    });

    chrome.action.setIcon({
      path: `/mpoof-${
        candidatesHidden.seen || candidatesHidden.contacted ? "" : "not-"
      }active-logo.png`,
    });
  }
}

export const getTabUrl: Promise<string | undefined> = new Promise((res) => {
  chrome.tabs?.query({ active: true, currentWindow: true }, (tabs) => {
    res(tabs[0].url);
  });
});

export function updateTabUrl() {
  chrome.tabs.update({ url: EXTENSION_PERMITTED_URL + "/talent/home" });
}
