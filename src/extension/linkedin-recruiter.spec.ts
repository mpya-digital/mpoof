import { chrome } from "jest-chrome";
import { linkedInRecruiter } from "./linkedin-recruiter";

describe("LinkedinRecruiter", () => {
  beforeEach(() => {
    jest.resetModules();
  });

  it('should get storage with key "candidatesHidden"', () => {
    localStorage.setItem(
      "candidatesHidden",
      JSON.stringify({ contacted: true, seen: false })
    );

    const candidatesHidden = linkedInRecruiter.getStorage("candidatesHidden");
    expect(candidatesHidden).toEqual({ contacted: true, seen: false });
  });

  it('should get storage with empyt object if "candidatesHidden" is not set', () => {
    localStorage.clear();

    const candidatesHidden = linkedInRecruiter.getStorage("candidatesHidden");
    expect(candidatesHidden).toEqual({});
  });

  it("should hide contacted candidates if candidates are present and candidate contacted storage is true", () => {
    localStorage.setItem(
      "candidatesHidden",
      JSON.stringify({ contacted: true, seen: false })
    );

    const candidatesList = document.createElement("ol");
    const candidate = document.createElement("li");
    candidatesList.appendChild(candidate);

    const messageButton = document.createElement("button");
    candidatesList.appendChild(candidate);

    jest
      .spyOn(candidate, "querySelector")
      .mockImplementation((_selector) => messageButton);
    jest
      .spyOn(document, "querySelector")
      .mockImplementation((_selector) => candidatesList);

    linkedInRecruiter.updateCandidatesIfPresent();

    expect(candidate.style.height).toEqual("0px");
    expect(candidate.style.transform).toEqual("scale(0)");
    expect(candidate.style.transition).toEqual("all 0.5s ease-in-out");
  });

  it('should set storage "candidatesHidden" with false on both values', () => {
    jest.spyOn(localStorage, "setItem");
    linkedInRecruiter.setStorage("candidatesHidden", {
      contacted: false,
      seen: false,
    });

    expect(localStorage.setItem).toHaveBeenCalledWith(
      "candidatesHidden",
      JSON.stringify({
        contacted: false,
        seen: false,
      })
    );
  });

  it("should add event listener to chrome message if init is called", () => {
    jest.spyOn(chrome.runtime.onMessage, "addListener");
    linkedInRecruiter.init();

    expect(chrome.runtime.onMessage.addListener).toHaveBeenCalled();
  });

  it("should update candidates if message is received ", () => {
    jest.spyOn(localStorage, "setItem");

    chrome.runtime.onMessage.callListeners(
      { contacted: true, seen: false },
      {},
      () => {}
    );
    expect(localStorage.setItem).toHaveBeenCalledWith(
      "candidatesHidden",
      JSON.stringify({ contacted: true, seen: false })
    );
  });

  it("should test node env", async () => {
    process.env = Object.assign(process.env, { NODE_ENV: "production" });
    jest.spyOn(window, "addEventListener");

    await import("./linkedin-recruiter");

    expect(window.addEventListener).toHaveBeenCalled();
  });
});
