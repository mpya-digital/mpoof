import { StorageName, StorageValue } from "../hooks/use-localstorage";
import {
  CANDIDATES_LIST_CLASSES,
  CANDIDATE_CONTACTED_CLASS,
  CANDIDATE_SEEN_CLASS,
  transitionDurationSec,
} from "./constants";

const getStorage = (storageName: StorageName): StorageValue => {
  const storageValue = localStorage.getItem(storageName);
  return storageValue ? JSON.parse(storageValue) : {};
};

const setStorage = (storageName: StorageName, storageValue: StorageValue) => {
  localStorage.setItem(storageName, JSON.stringify(storageValue));
};

const updateCandidates = (_candidates: HTMLCollection) => {
  const candidates = [..._candidates] as HTMLElement[];
  const candidatesHidden = getStorage("candidatesHidden");

  candidates.forEach((candidate) => {
    const contactedCandidate = candidate.querySelector(
      CANDIDATE_CONTACTED_CLASS
    );
    const seenCandidate = candidate.querySelector(CANDIDATE_SEEN_CLASS);

    if (contactedCandidate || seenCandidate) {
      if (
        (candidatesHidden.contacted && candidatesHidden.seen) ||
        (seenCandidate && candidatesHidden.seen) ||
        (contactedCandidate && candidatesHidden.contacted)
      ) {
        candidate.style.transition = `all ${transitionDurationSec}s ease-in-out`;
        candidate.style.transform = "scale(0)";
        candidate.style.height = "0";
      } else {
        candidate.style.removeProperty("transform");
        candidate.style.removeProperty("height");
        setTimeout(() => {
          candidate.removeAttribute("style");
        }, transitionDurationSec * 1000);
      }
    }
  });
};

const updateCandidatesIfPresent = () => {
  const candidatesList = document.querySelector(CANDIDATES_LIST_CLASSES);
  if (candidatesList) {
    updateCandidates(candidatesList.children);
  }
};

const observeDomChanges = () => {
  const observer = new MutationObserver(updateCandidatesIfPresent);

  observer.observe(document.body, {
    childList: true,
    subtree: true,
  });
};

const init = () => {
  chrome.runtime.onMessage.addListener((candidatesHidden: StorageValue) => {
    setStorage("candidatesHidden", candidatesHidden);
    updateCandidatesIfPresent();
  });

  observeDomChanges();
};

export let linkedInRecruiter: {
  getStorage: typeof getStorage;
  setStorage: typeof setStorage;
  init: typeof init;
  updateCandidatesIfPresent: typeof updateCandidatesIfPresent;
  CANDIDATES_LIST_CLASSES: typeof CANDIDATES_LIST_CLASSES;
  CANDIDATE_CONTACTED_CLASS: typeof CANDIDATE_CONTACTED_CLASS;
};

if (process.env.NODE_ENV === "production") {
  window.addEventListener("load", init, false);
} else {
  linkedInRecruiter = {
    getStorage,
    setStorage,
    init,
    updateCandidatesIfPresent,
    CANDIDATE_CONTACTED_CLASS,
    CANDIDATES_LIST_CLASSES,
  };
}
