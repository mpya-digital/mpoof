import { chrome } from "jest-chrome";
import { updateExtension, updateTabUrl } from "./chrome";

describe("Chrome", () => {
  it("should send message to tab if updateExtension is called", () => {
    jest.spyOn(chrome.tabs, "query");
    updateExtension({ contacted: true, seen: false });
    expect(chrome.tabs.query).toHaveBeenCalled();
  });

  it("should send message if callback is called with tabs with id", () => {
    jest.spyOn(chrome.tabs, "sendMessage");
    updateExtension({ contacted: true, seen: false });

    (chrome.tabs.query.mock.calls[0] as any)[1]([{ id: 637 }]);
    expect(chrome.tabs.sendMessage).toBeCalledWith(637, {
      contacted: true,
      seen: false,
    });
  });

  it("should not send message if callback is called with tabs without id", () => {
    jest.spyOn(chrome.tabs, "sendMessage");
    updateExtension({ contacted: true, seen: false });

    (chrome.tabs.query.mock.calls[0] as any)[1]([{}]);
    expect(chrome.tabs.sendMessage).toHaveBeenCalledTimes(0);
  });

  it("should update tab url", async () => {
    jest.spyOn(chrome.tabs, "update");
    updateTabUrl();

    expect(chrome.tabs.update).toHaveBeenCalledWith({
      url: "https://www.linkedin.com/talent/home",
    });
  });
});
